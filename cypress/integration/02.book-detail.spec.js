/// <reference types="Cypress" />

describe('Navigation between List & Detail views', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  it('should let us click on a book for more details and return back to list', ()=> {
    cy.get('.book')
      .should('have.length', 7)
      .contains('Azkaban')
      .click()
      .get('.book')
      .should('have.length', 1)
      .should('contain', 'Henri Potier et le Prisonnier d\'Azkaban')
      .get('button[test-id^=return]')
      .click()
      .location().should((location) => {
        expect(location.pathname).to.eq('/books')
      })
      .get('.book')
      .should('have.length', 7)
      .end();
  });
});

