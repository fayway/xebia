/// <reference types="Cypress" />

describe('Books Collection Page', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  it('should load collection', ()=> {
    cy.title().should('include', 'Henri Potier');

    cy.get('.book').should('have.length', 7)
      .first().should('contain', "Henri Potier à l'école des sorciers")
      .next().should('contain', 'Henri Potier et la Chambre des secrets')
      .next().should('contain', "Henri Potier et le Prisonnier d'Azkaban")
      .next().should('contain', 'Henri Potier et la Coupe de feu')
      .next().should('contain', "Henri Potier et l'Ordre du phénix")
      .next().should('contain', 'Henri Potier et le Prince de sang-mêlé')
      .next().should('contain', 'Henri Potier et les Reliques de la Mort');
  });

  it('should filter books and highlight keywords', ()=> {
    cy.get('.book')
      .should('have.length', 7)
      .get('input[type=search]')
      .type('azk')
      .wait(350)
      .get('.book').should('have.length', 1)
      .first().should('contain', 'Azkaban')
      .contains('Azk').should('have.class', 'search-highlight');

    cy.get('input[type=search]')
      .type('{selectall}{del}')
      .wait(350)
      .get('.book').should('have.length', 7)
      .end();
  })
});

