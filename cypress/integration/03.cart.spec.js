/// <reference types="Cypress" />

describe('Cart', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  it('should add books to cart', ()=> {
    cy.get('h1').should('contain', 'Collection')
      .get('.mat-badge-content').should('contain', 0)
      .get('.book')
      .contains(/Henri Potier à l'école des sorciers/)
      .parentsUntil('.book')
      .within(() => {
        cy.get('.mat-raised-button[test-id^=add]').click();
      })
      .get('.book')
      .contains(/Henri Potier et la Chambre des secrets/)
      .parentsUntil('.book')
      .within(() => {
        cy.get('.mat-raised-button[test-id^=add]').click();
        cy.get('.mat-raised-button[test-id^=add]').click();
      })
      .get('.mat-badge-content').should('contain', 3)
      .get('a[test-id=cart]')
      .click()
      .get('h1').should('contain', 'Panier')
      .get('.cartItem').should('have.length', 2)
      .get('.discount')
      .should('contain', '15,00')
      .get('.payment-info').should('contain', '80,00')
      .get('button[test-id^=incr]').eq(0)
      .click()
      .get('button[test-id^=incr]').eq(0)
      .click()
      .get('.mat-badge-content').should('contain', 5)
      .get('button[test-id^=incr]').eq(1)
      .click()
      .get('button[test-id^=incr]').eq(1)
      .click()
      .get('.mat-badge-content').should('contain', 7)
      .get('button[test-id^=decr]').eq(0)
      .click()
      .get('.mat-badge-content').should('contain', 6)
      .get('.discount').should('contain', '30,00')
      .get('.payment-info').should('contain', '160,00')
      .get('button[test-id^=rm]').eq(0)
      .click()
      .get('.mat-badge-content').should('contain', 4)
      .get('.discount').should('contain', '30,00')
      .get('.payment-info').should('contain', '90,00')
      .end()
  })
});

