# La bibliothèque de Henri Potier Implémentation

## Démarrage

`git clone https://fayway@bitbucket.org/fayway/xebia.git`

`cd xebia`

`yarn` (ou `npm install` si vous voulez faire une pause-café   
:wink: )

`yarn start`

![La Home]( https://bitbucket.org/fayway/xebia/raw/7ccf75f8679316986ed5811ee70755d4d9ffac12/screenshots/home.png)

# Le Domain

Les fichiers domaines sont localisés dans le dossier `domain`: [https://bitbucket.org/fayway/xebia/src/master/src/app/domain/](https://bitbucket.org/fayway/xebia/src/master/src/app/domain/)

## Le diagramme de classes du domaine

![Diagramme de classe](https://bitbucket.org/fayway/xebia/raw/9065471dfd6f582bc517372172d5c4fc273dc1dd/src/app/domain/diagram.class.png)
[Diagramme de classe](https://yuml.me/edit/361f6161)

# Tests automatisés

Dans ce projet Angular, j’ai voulu expérimenter d’autres outils plus "developer-friendly" à savoir **Jest** et **Cypress**

## Tous les tests unitaires et d’intégrations :

`yarn test`

## Uniquement les tests unitaire du domain

`yarn test:domain`

## Tests avec taux de couverture

`yarn test:coverage`

![Taux de coverture](https://bitbucket.org/fayway/xebia/raw/9065471dfd6f582bc517372172d5c4fc273dc1dd/screenshots/coverage.png)

## Tests E2E avec Cypress

### Avec le client Electron

`yarn e2e` (Il faut que l'application soit démarrée `yarn start` )

### En mode console

`yarn e2e:headless`

### Si vous êtes derrière un proxy

`yarn e2e:proxy` (Mettre à jour le proxy dans la [package.json]( https://bitbucket.org/fayway/xebia/src/474933dc829910ba62ae928ec3434b2a0c957eea/package.json#lines-15))

![Tests E2E avec Cypress]( https://bitbucket.org/fayway/xebia/raw/7ccf75f8679316986ed5811ee70755d4d9ffac12/screenshots/cypress.png)