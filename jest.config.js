module.exports = {
  preset: 'jest-preset-angular',
  roots: ['src'],
  setupTestFrameworkScriptFile: '<rootDir>/src/setup-jest.ts',
  moduleNameMapper: {
    '@app/(.*)': '<rootDir>/src/app/$1',
    '@domain/(.*)': '<rootDir>/src/app/domain/$1',
    '@application/(.*)': '<rootDir>/src/app/application/$1',
    '@infrastructure/(.*)': '<rootDir>/src/app/infrastructure/$1',
    '@env/(.*)': '<rootDir>/src/environments/$1',
  },
  coveragePathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/coverage/',
    '<rootDir>/e2e/',
    '<rootDir>/e2e/',
    '<rootDir>/src/jestGlobalMocks.ts',
    '<rootDir>/src/setup-jest.ts',
  ]
};
