// Back Domain

// Domain Model
export enum OfferType {
  PERCENTAGE = 'percentage',
  MINUS = 'minus',
  SLICE = 'slice',
}

export interface OfferInterface {
  getType(): OfferType;
  getDiscount(price?: number): number;
}

export abstract class AbstractOffer implements OfferInterface {
  protected constructor(private _type: OfferType) {
  }
  abstract getDiscount(price?: number): number;
  getType(): OfferType {
    return this._type;
  }
}

export interface CommercialOffersInterface {
  offers: OfferInterface[];
}

export class CommercialOffers implements CommercialOffersInterface {
  get offers() {
    return this._offers;
  }
  constructor(private _offers: OfferInterface[]) {
  }
  getOfferByType(type: OfferType): OfferInterface {
    return this._offers.find(offer => offer.getType() === type);
  }
  getBestOffer(price: number): OfferInterface {
    return this._offers.reduce((previousBestOffer: OfferInterface, currentOffer: OfferInterface) => {
      if (!previousBestOffer) {
        return currentOffer;
      }
      if (previousBestOffer.getDiscount(price) < currentOffer.getDiscount(price)) {
        return currentOffer;
      }
      return previousBestOffer;
    }, null);
  }
}

// Can't use "OfferError extends Error" because I can't use a Babel plugin without ng eject
// https://github.com/facebook/jest/issues/2123
export const OfferError = (message: string) => {};
OfferError.prototype = Object.create(Error.prototype);
