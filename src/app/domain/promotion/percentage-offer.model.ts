import { AbstractOffer, OfferType } from './offer.model';

export class PercentageOffer extends AbstractOffer {
  private readonly _value: number;
  get value(): number {
    return this._value;
  }

  constructor(value: number) {
    super(OfferType.PERCENTAGE);
    this._value = value;
  }

  getDiscount(price: number) {
    return price * this.value / 100;
  }
}
