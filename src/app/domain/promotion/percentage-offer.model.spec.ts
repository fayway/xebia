import { PercentageOffer } from './percentage-offer.model';

describe('PercentageOffer Domain', () => {

  test('getDiscount()', () => {
    const offer1 = new PercentageOffer(5);
    expect(offer1.getDiscount(65)).toBeCloseTo(3.25);

    const offer2 = new PercentageOffer(5);
    expect(offer2.getDiscount(100)).toBeCloseTo(5);

    const offer3 = new PercentageOffer(10);
    expect(offer3.getDiscount(100)).toBeCloseTo(10);
  });

});
