import { MinusOffer } from './minus-offer.model';

describe('MinusOffer Domain', () => {

  test('getDiscount()', () => {
    const offer1 = new MinusOffer(15);
    expect(offer1.getDiscount()).toBeCloseTo(15);

    const offer2 = new MinusOffer(10);
    expect(offer2.getDiscount()).toBeCloseTo(10);
  });

});
