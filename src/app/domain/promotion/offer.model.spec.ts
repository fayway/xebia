import { CommercialOffers, OfferType } from './offer.model';
import { PercentageOffer } from './percentage-offer.model';
import { MinusOffer } from './minus-offer.model';
import { SliceOffer } from './slice-offer.model';

describe('CommercialOffers Domain', () => {

  const price = 65;

  const percentageOffer = new PercentageOffer(5);
  const minusOffer = new MinusOffer(15);
  const sliceOffer = new SliceOffer(100, 12);

  const commercialOffers = new CommercialOffers([
    percentageOffer,
    minusOffer,
    sliceOffer
  ]);

  test('getBestOffer()', () => {
    expect(commercialOffers.getBestOffer(price)).toEqual(minusOffer);
  });

  test('getOfferByType()', () => {
    expect(commercialOffers.getOfferByType(OfferType.PERCENTAGE)).toEqual(percentageOffer);
    expect(commercialOffers.getOfferByType(OfferType.MINUS)).toEqual(minusOffer);
    expect(commercialOffers.getOfferByType(OfferType.SLICE)).toEqual(sliceOffer);
  });

});
