import { AbstractOffer, OfferError, OfferType } from './offer.model';

export class MinusOffer extends AbstractOffer {
  private readonly _value: number;
  get value(): number {
    return this._value;
  }

  constructor(value: number) {
    super(OfferType.MINUS);
    this._value = value;
  }

  getDiscount() {
    return this.value;
  }
}
