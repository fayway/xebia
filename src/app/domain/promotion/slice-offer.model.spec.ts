import { SliceOffer } from './slice-offer.model';

describe('SliceOffer Domain', () => {

  test('getDiscount()', () => {
    const offer1 = new SliceOffer(100, 12);
    expect(offer1.getDiscount(65)).toBeCloseTo(0);

    const offer2 = new SliceOffer(100, 12);
    expect(offer2.getDiscount(120)).toBeCloseTo(12);

    const offer3 = new SliceOffer(100, 12);
    expect(offer3.getDiscount(250)).toBeCloseTo(24);
  });

});

