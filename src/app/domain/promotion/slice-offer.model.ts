import { AbstractOffer, OfferType } from './offer.model';

export class SliceOffer extends AbstractOffer {
  private readonly _sliceValue: number;
  private readonly _value: number;

  get sliceValue(): number {
    return this._sliceValue;
  }

  get value(): number {
    return this._value;
  }

  constructor(sliceValue: number, value: number) {
    super(OfferType.SLICE);
    this._sliceValue = sliceValue;
    this._value = value;
  }

  getDiscount(price: number) {
    return Math.floor(price / this.sliceValue) * this.value;
  }
}
