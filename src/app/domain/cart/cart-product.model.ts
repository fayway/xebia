import { Product } from '@domain/product/product.model';

export interface CartProductInterface {
  product: Product;
  quantity: number;
}

export class CartProduct implements CartProductInterface {
  get product(): Product {
    return this._product;
  }

  get quantity(): number {
    return this._quantity;
  }

  static adapt(obj: CartProductInterface): CartProduct {
    return new CartProduct(obj.product, obj.quantity);
  }

  constructor(private _product: Product, private _quantity: number) {
  }

  getSubTotal(): number {
    return this._product.price * this._quantity;
  }
}