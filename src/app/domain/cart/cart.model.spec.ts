import { CartProduct } from './cart-product.model';
import { Cart } from './cart.model';
import { CommercialOffers, OfferType } from '@domain/promotion/offer.model';
import { PercentageOffer } from '@domain/promotion/percentage-offer.model';
import { MinusOffer } from '@domain/promotion/minus-offer.model';
import { SliceOffer } from '@domain/promotion/slice-offer.model';
import { Book } from '@domain/catalog/item.model';

describe('Cart Domain', () => {
  const commercialOffers = new CommercialOffers([
    new PercentageOffer(5),
    new MinusOffer(15),
    new SliceOffer(100, 12)
  ]);

  const book1 = new Book('isbn1', 30);
  const book2 = new Book('isbn2', 35);

  let cart: Cart;

  beforeEach(() => {
    const cartProducts = [
      new CartProduct(book1, 1),
      new CartProduct(book2, 1),
    ];
    cart = new Cart(cartProducts, commercialOffers);
  });

  test('getTotal()', () => {
    expect(cart.getTotal()).toBeCloseTo(65);
  });

  test('getBestOffer()', () => {
    expect(cart.getBestOffer()).toEqual(commercialOffers.getOfferByType(OfferType.MINUS));
  });

  test('getTotalWithDiscount()', () => {
    expect(cart.getTotalWithDiscount()).toBeCloseTo(50);
  });

  test('incrementProduct() add Product if doesn\'t exist', () => {
    const cartProducts = [
      new CartProduct(book1, 1),
    ];
    const expectedCartProducts = Cart.incrementProduct(cartProducts, book2);
    expect(expectedCartProducts.length).toBe(2);
    const addedProduct = expectedCartProducts.find(cartItem => cartItem.product.getId() === book2.getId());
    expect(addedProduct.quantity).toBe(1);
  });

  test('incrementProduct() increments quantity if Product already exists', () => {
    const cartProducts = [
      new CartProduct(book1, 1),
      new CartProduct(book2, 1),
    ];
    const expectedCartProducts = Cart.incrementProduct(cartProducts, book1);
    expect(expectedCartProducts.length).toBe(2);
    const cartProduct = expectedCartProducts.find(cartItem => cartItem.product.getId() === book1.getId());
    expect(cartProduct.quantity).toBe(2);
  });

  test('decrementProduct() removes an Product when quantity becomes 0', () => {
    const cartProducts = [
      new CartProduct(book1, 1),
      new CartProduct(book2, 1),
    ];
    const expectedCartProducts = Cart.decrementProduct(cartProducts, book1);
    expect(expectedCartProducts.length).toBe(1);
    const decrementedCartProduct = expectedCartProducts.find(cartItem => cartItem.product.getId() === book1.getId());
    expect(decrementedCartProduct).toBeFalsy();
  });

  test('decrementProduct() decrements Product quantity as expected', () => {
    const cartProducts = [
      new CartProduct(book1, 2),
      new CartProduct(book2, 1),
    ];
    const expectedCartProducts = Cart.decrementProduct(cartProducts, book1);
    expect(expectedCartProducts.length).toBe(2);
    const decrementedProduct = expectedCartProducts.find(cartItem => cartItem.product.getId() === book1.getId());
    expect(decrementedProduct.quantity).toBe(1);
  });

});
