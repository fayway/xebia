import { CommercialOffers, CommercialOffersInterface, OfferInterface } from '@domain/promotion/offer.model';
import { CartProduct} from './cart-product.model';
import { Product } from '@domain/product/product.model';
import { CartProductInterface } from '@domain/cart/cart-product.model';

export interface CartInterface {
  cartProducts: CartProductInterface[];
  commercialOffers: CommercialOffersInterface;
}

export class Cart implements CartInterface {
  get cartProducts() {
    return this._cartProducts;
  }

  get commercialOffers() {
    return this._commercialOffers;
  }

  set commercialOffers(commercialOffers: CommercialOffers) {
    this._commercialOffers = commercialOffers;
  }

  static incrementProduct(cartProducts: CartProductInterface[], product: Product): CartProductInterface[] {
    const targetIndex = cartProducts.findIndex(elem => elem.product.getId() === product.getId());
    if (targetIndex === -1) {
      return [
        ...cartProducts,
        {product: product, quantity: 1}
      ];
    }
    // else
    return cartProducts.map((elem, index) => {
      if (index !== targetIndex) {
        return elem;
      } else {
        return new CartProduct(elem.product, elem.quantity + 1);
      }
    });
  }

  static decrementProduct(cartProducts: CartProductInterface[], product: Product): CartProductInterface[] {
    const targetIndex = cartProducts.findIndex(elem => elem.product.getId() === product.getId());
    if (targetIndex === -1) {
      return [...cartProducts];
    }
    // else
    if (cartProducts[targetIndex].quantity === 1) {
      // delete product from cartProducts
      return [
        ...cartProducts.slice(0, targetIndex),
        ...cartProducts.slice(targetIndex + 1)
      ];
    }
    // else
    return cartProducts.map((elem, index) => {
      if (index !== targetIndex) {
        return elem;
      } else {
        return new CartProduct(elem.product, elem.quantity - 1);
      }
    });
  }

  static removeProduct(cartProducts: CartProductInterface[], product: Product): CartProductInterface[] {
    return cartProducts.filter(elem => elem.product.getId() !== product.getId());
  }

  constructor(private _cartProducts: CartProduct[], private _commercialOffers: CommercialOffers = null) {
  }

  getTotal(): number {
    return this._cartProducts.reduce((total: number, currentProduct: CartProduct) => {
      return total + currentProduct.getSubTotal();
    }, 0);
  }

  getBestOffer(): OfferInterface {
    if (this._commercialOffers) {
      return this._commercialOffers.getBestOffer(this.getTotal());
    }
    return null;
  }

  getDiscount(): number {
    return this.getBestOffer() ? this.getBestOffer().getDiscount(this.getTotal()) : 0;
  }

  getTotalWithDiscount(): number {
    return this.getTotal() - this.getDiscount();
  }
}
