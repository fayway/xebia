import { Product } from '@domain/product/product.model';
import { BookInterface } from '@domain/library/book.model';

export class Book implements BookInterface, Product {
  isbn: string;
  title: string;
  price: number;
  cover: string;
  synopsis: string[];

  constructor(isbn: string, price: number, title: string = null, cover: string = null, synopsis: string[] = []) {
    this.isbn = isbn;
    this.price = price;
    this.title = title;
    this.cover = cover;
    this.synopsis = synopsis;
  }

  getId(): string {
    return this.isbn;
  }
}
