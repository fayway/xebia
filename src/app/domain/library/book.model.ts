export interface BookInterface {
  isbn: string;
  title: string;
  cover: string;
  synopsis: string[];
}
