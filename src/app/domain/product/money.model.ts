import { Currency } from '@app/domain/product/currency.model';

export class Money {
  amount: number;
  currency: Currency;
}
