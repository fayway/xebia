import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotfoundPageComponent } from '@infrastructure/core/shell/pages/404/notfound-page/notfound-page.component';
import { BooksListPageComponent } from '@infrastructure/books/containers/books-list-page/books-list-page.component';
import { BookDetailPageComponent } from '@infrastructure/books/containers/book-detail-page/book-detail-page.component';
import { CartPageComponent } from '@infrastructure/shop/containers/cart-page/cart-page.component';
import { CollectionPreloadGuard } from '@infrastructure/books/guards/collection-preload.guard';
import { BookExistenceGuard } from '@infrastructure/books/guards/book-existence.guard';

export const routes: Routes = [
  { path: '', redirectTo: '/books', pathMatch: 'full' },
  { path: 'books', component: BooksListPageComponent, canActivate: [CollectionPreloadGuard] },
  { path: 'book/:isbn', component: BookDetailPageComponent, canActivate: [BookExistenceGuard] },
  { path: 'cart', component: CartPageComponent },
  { path: '404', component: NotfoundPageComponent },
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule { }
