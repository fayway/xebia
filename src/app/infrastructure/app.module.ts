import {LOCALE_ID, NgModule} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { AppStoreModule } from './app-store.module';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from '@infrastructure/core/core.module';
import { ShellComponent } from '@infrastructure/core/shell/containers/shell.component';
import { BooksModule } from '@infrastructure/books/books.module';
import { ShopModule } from '@infrastructure/shop/shop.module';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
  ],
  imports: [
    AppRoutingModule,
    AppStoreModule,
    BooksModule,
    ShopModule,
    CoreModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [ShellComponent]
})
export class AppModule { }
