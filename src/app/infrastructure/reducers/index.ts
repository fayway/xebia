import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { RouterStateUrl } from '@infrastructure/core/utils/custom-router-state-serializer';
import { environment } from '@env/environment';

export interface State {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [storeFreeze]
  : [];
