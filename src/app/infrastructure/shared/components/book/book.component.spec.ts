import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookComponent } from './book.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Book } from '@domain/catalog/item.model';
import { HighlightPipe } from '@infrastructure/shared/pipes/highlight.pipe';
import { MaterialModule } from '@infrastructure/shared/material/material.module';

describe('BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;
  const book1 = new Book('123', 45.32, 'Book Title', 'http://cover.com', ['Lorem Ipsum']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookComponent, HighlightPipe],
      imports: [MaterialModule, RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;

    spyOn(component.goToDetail, 'emit').and.callThrough();
    spyOn(component.goBack, 'emit').and.callThrough();
    spyOn(component.add, 'emit').and.callThrough();

    component.book = book1;
    component.showDescription = true;
    component.fullDescription = true;
    component.showCover = true;
    component.showAddAction = true;
    component.showBackAction = true;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(fixture).toMatchSnapshot();
  });

  it('should add.emit() event on click', () => {
    fixture.detectChanges();
    const bookElement = fixture.nativeElement;
    //
    const addBtn: HTMLButtonElement = bookElement.querySelector(`button[test-id="add:${book1.isbn}"]`);
    addBtn.click();
    expect(component.add.emit).toHaveBeenCalled();
  });

  it('should goBack.emit() event on click', () => {
    fixture.detectChanges();
    const bookElement = fixture.nativeElement;
    //
    const returnBtn: HTMLButtonElement = bookElement.querySelector(`button[test-id="return:${book1.isbn}"]`);
    returnBtn.click();
    expect(component.goBack.emit).toHaveBeenCalled();
  });

  it('should emit() events on click', () => {
    component.fullDescription = false;
    fixture.detectChanges();
    const bookElement = fixture.nativeElement;
    const returnBtn: HTMLButtonElement = bookElement.querySelector(`button[test-id="detail:${book1.isbn}"]`);
    returnBtn.click();
    expect(component.goToDetail.emit).toHaveBeenCalled();
  });
});
