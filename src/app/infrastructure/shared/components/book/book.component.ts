import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Book } from '@domain/catalog/item.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  @Input() book: Book;
  @Input() showDescription = false;
  @Input() fullDescription = true;
  @Input() showCover = false;
  @Input() floatCover = false;
  @Input() showBackAction = false;
  @Input() showAddAction = false;
  @Input() query: string;

  @Output() goToDetail = new EventEmitter();
  @Output() goBack = new EventEmitter();
  @Output() add = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
