import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BooksListPageComponent } from './containers/books-list-page/books-list-page.component';
import { BookDetailPageComponent } from './containers/book-detail-page/book-detail-page.component';
import { CollectionEffects } from './effects/collection-effects.service';
import { reducers } from './reducers/index';
import { CollectionPreloadGuard } from './guards/collection-preload.guard';
import { BookExistenceGuard } from './guards/book-existence.guard';
import { BookStoreService } from './services/book-store.service';
import { SearchEffects } from './effects/search-effects.service';
import { SharedModule } from '@infrastructure/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    StoreModule.forFeature('books', reducers),
    EffectsModule.forFeature([CollectionEffects, SearchEffects]),
  ],
  declarations: [BooksListPageComponent, BookDetailPageComponent],
  providers: [BookStoreService, CollectionPreloadGuard, BookExistenceGuard]
})
export class BooksModule { }
