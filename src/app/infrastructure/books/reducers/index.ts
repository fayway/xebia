import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../reducers/index';
import * as fromCollection from './collection.reducer';
import * as fromSearch from './search.reducer';
import { BookAdapter } from '@application/product/book.adapter';

export interface BooksState {
  collection: fromCollection.State;
  search: fromSearch.State;
}

export interface State extends fromRoot.State {
  books: BooksState;
}

export const reducers: ActionReducerMap<BooksState> = {
  collection: fromCollection.reducer,
  search: fromSearch.reducer,
};

export const getBooksFeature = createFeatureSelector<BooksState>('books');

// Books Collection
export const getCollectionState = createSelector(
  getBooksFeature,
  state => state.collection
);

export const getCollectionLoadingState = createSelector(
  getCollectionState,
  state => ({loaded: state.loaded, loading: state.loading})
);

export const getCollectionEntities = createSelector(
  getCollectionState,
  state => state.entities
);

export const getFlatCollectionEntities = createSelector(
  getCollectionEntities,
  entities => Object.keys(entities).map(isbn => entities[isbn])
);

export const getBook = isbn => createSelector(
  getCollectionState,
  state => state.entities[isbn]
);

// Search
export const getSearchState = createSelector(
  getBooksFeature,
  state => state.search
);

export const getSearchQuery = createSelector(
  getSearchState,
  state => state.query
);

export const getSearchResultIds = createSelector(
  getSearchState,
  state => state.ids
);

export const getFiltredBooks = createSelector(
  getCollectionEntities,
  getSearchQuery,
  getSearchResultIds,
  (entities, query, ids) => {
    if (!query) {
      return Object.keys(entities)
        .map(isbn => entities[isbn]);
    }
    return ids
      .map(id => entities[id]);
  }
);

export const mapBookToDomain = book => BookAdapter.mapToDomain(book);
export const mapBooksToDomain = books => books.map(book => mapBookToDomain(book));

export const getSearchLoadingState = createSelector(
  getSearchState,
  state => ({loaded: state.loaded, loading: state.loading})
);

export const isLoading = createSelector(
  getCollectionLoadingState,
  getSearchLoadingState,
  ((collectionLoading, searchLoading) => {
    return collectionLoading.loading || searchLoading.loading;
  })
);
