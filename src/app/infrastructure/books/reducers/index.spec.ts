import { Book } from '@domain/catalog/item.model';
import { initialState } from './collection.reducer';
import { getBook, getCollectionLoadingState, getFiltredBooks, getFlatCollectionEntities, isLoading } from './index';

describe('Books Selectors', () => {
  const book1 = new Book('123', 12, 'Book 123');
  const book2 = new Book('456', 36, 'Book 456');
  const entities = {
    [book1.isbn]: book1,
    [book2.isbn]: book2
  };

  it('getFiltredBooks should return a derived state according to query', function () {
    expect(getFiltredBooks.projector(entities, '', [book1.isbn])).toEqual([book1, book2]);
    expect(getFiltredBooks.projector(entities, 'book 123', [book1.isbn, book2.isbn])).toEqual([book1, book2]);
    expect(getFiltredBooks.projector(entities, 'book 123', [book1.isbn])).toEqual([book1]);
  });

  it('getCollectionLoadingState should return a derived state with loading state', function () {
    expect(getCollectionLoadingState.projector(initialState)).toMatchSnapshot();
  });

  it('getFlatCollectionEntities should return a derived state with flat books array', function () {
    expect(getFlatCollectionEntities.projector(entities)).toMatchSnapshot();
  });

  it('getBook should select the correct book by its id', function () {
    expect(getBook(book1.isbn).projector({entities})).toEqual(book1);
  });

  it('isLoading should select the correct book by its id', function () {
    const loadingState = {loaded: true, loading: false};
    expect(isLoading.projector(loadingState, loadingState)).toMatchSnapshot();
  });

});
