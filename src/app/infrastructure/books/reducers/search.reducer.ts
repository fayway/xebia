import { SearchActions, SearchActionTypes } from '../actions/search.actions';

export interface State {
  ids: string[];
  query: string;
  loading: boolean;
  loaded: boolean;
  error: Error;
}

export const initialState: State = {
  ids: [],
  query: '',
  loading: false,
  loaded: false,
  error: null
};

export function reducer(state = initialState, action: SearchActions): State {
  switch (action.type) {

    case SearchActionTypes.SearchCollectionRequest:
      return {
        ...state,
        ids: [],
        query: action.payload.query,
        loading: true,
        loaded: false,
        error: null
      };
    case SearchActionTypes.SearchCollectionSuccess:
      return {
        ...state,
        ids: action.payload.books.map(book => book.isbn),
        loading: false,
        loaded: true,
        error: null
      };
    case SearchActionTypes.SearchCollectionFailure:
      return {
        ...state,
        ids: [],
        loading: false,
        loaded: true,
        error: action.payload.error,
      };

    default:
      return state;
  }
}
