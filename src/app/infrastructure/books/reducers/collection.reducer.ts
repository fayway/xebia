import { CollectionActions, CollectionActionTypes } from '../actions/collection.actions';
import { ExternalBookInterface } from '@application/product/external-book.interface';

export interface State {
  ids: string[];
  entities: {[id: string]: ExternalBookInterface};
  loading: boolean;
  loaded: boolean;
  error: Error;
}

export const initialState: State = {
  ids: [],
  entities: {},
  loading: false,
  loaded: false,
  error: null,
};

export function reducer(state = initialState, action: CollectionActions): State {
  switch (action.type) {

    case CollectionActionTypes.LoadCollectionRequest:
      return {
        ...state,
        entities: {},
        loading: true,
        error: null,
      };
    case CollectionActionTypes.LoadCollectionSuccess:
      return {
        ...state,
        ids: action.payload.books.map(book => book.isbn),
        entities: action.payload.books.reduce((acc, book) => {
          acc[book.isbn] = book;
          return acc;
        }, {}),
        loading: false,
        loaded: true,
        error: null,
      };
    case CollectionActionTypes.LoadCollectionFailure:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: true
      };

    default:
      return state;
  }
}
