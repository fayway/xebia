import { reducer, initialState } from './collection.reducer';
import { LoadCollectionFailure, LoadCollectionRequest, LoadCollectionSucess } from '../actions/collection.actions';
import { ExternalBookInterface } from '@application/product/external-book.interface';

describe('Collection Reducer', () => {

  const book1 = <ExternalBookInterface>{isbn: '123', price: 25, title: 'Book 123', cover: 'http://cover1.com', synopsis: ['Lorem ipsum']};
  const book2 = <ExternalBookInterface>{isbn: '456', price: 30, title: 'Book 456', cover: 'http://cover.com', synopsis: ['Lorem ipsum']};

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('LoadCollectionRequest action', () => {
    it('should return the initial state with loading=true', () => {
      const result = reducer(initialState, new LoadCollectionRequest());
      expect(result).toMatchSnapshot();
    });
  });

  describe('LoadCollectionSucess action', () => {
    it('should return the state with loaded books', () => {
      const result = reducer(initialState, new LoadCollectionSucess({books: [book1, book2]}));
      expect(result).toMatchSnapshot();
    });
  });

  describe('LoadCollectionFailure action', () => {
    it('should return the state with error details', () => {
      const result = reducer(initialState, new LoadCollectionFailure({error: new Error('Ouch Charlie')}));
      expect(result).toMatchSnapshot();
    });
  });
});
