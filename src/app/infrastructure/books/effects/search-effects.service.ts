import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { SearchActionTypes, SearchFailure, SearchRequest, SearchSucess } from '../actions/search.actions';
import * as fromBooks from '../reducers/index';
import { BooksService } from '../services/books.service';

@Injectable()
export class SearchEffects {

  @Effect()
  search$ = this.actions$
    .ofType(SearchActionTypes.SearchCollectionRequest)
    .pipe(
      map((action: SearchRequest) => action.payload.query),
      withLatestFrom(this.store.select(fromBooks.getFlatCollectionEntities)),
      switchMap(([query, books]) => {
        return this.bookService.search(books, query)
          .pipe(
            map((filtredBooks) => new SearchSucess({books: filtredBooks})),
            catchError((error: Error) => of(new SearchFailure({error})))
          );
      })
    );

  constructor(
    private actions$: Actions,
    private bookService: BooksService,
    private store: Store<fromBooks.State>
  ) {}
}
