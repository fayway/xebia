import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { Book } from '@domain/catalog/item.model';
import { Store } from '@ngrx/store';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SearchEffects } from './search-effects.service';
import { SearchFailure, SearchRequest, SearchSucess } from '../actions/search.actions';
import { BooksService } from '../services/books.service';

describe('SearchEffects', () => {
  let actions$: Observable<any>;
  let effects: SearchEffects;
  let booksService: BooksService;
  const book1 = new Book('123', 12, 'Book 123');
  const book2 = new Book('456', 36, 'Book 456');
  const books = [book1, book2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        SearchEffects,
        BooksService,
        provideMockActions(() => actions$),
        {
          provide: Store,
          useValue: {
            select: () => {
              return of(books);
            }
          }
        }
      ]
    });

    booksService = TestBed.get(BooksService);

    effects = TestBed.get(SearchEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should dispatch SearchSucess with filtred Books after a SearchRequest', () => {
    spyOn(booksService, 'search').and.callThrough();
    const action = new SearchRequest({query: '123'});
    const completion = new SearchSucess({books: [book1]});

    actions$ = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.search$).toBeObservable(expected);
    expect(booksService.search).toHaveBeenCalledWith(books, '123');
  });

  it('should dispatch SearchFailure when BooksService.search does the same', () => {
    const error = new Error('Ouch Charlie!');
    spyOn(booksService, 'search').and.returnValue(throwError(error));

    const action = new SearchRequest({query: '123'});
    const completion = new SearchFailure({error});

    actions$ = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.search$).toBeObservable(expected);
  });

});
