import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { catchError, concatMap, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { CollectionActionTypes, LoadCollectionFailure, LoadCollectionSucess } from '../actions/collection.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { BooksService } from '../services/books.service';

@Injectable()
export class CollectionEffects {

  @Effect()
  load$ = this.actions$
    .ofType(CollectionActionTypes.LoadCollectionRequest)
    .pipe(
      concatMap(() => this.bookService.getBooks()
        .pipe(
          map((books) => new LoadCollectionSucess({books})),
          catchError((response: HttpErrorResponse) => of(new LoadCollectionFailure({error: response})))
      ))
    );

  constructor(private actions$: Actions, private bookService: BooksService) {}
}
