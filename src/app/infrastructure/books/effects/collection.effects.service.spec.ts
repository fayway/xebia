import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of, throwError } from 'rxjs';

import { CollectionEffects } from './collection-effects.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { cold, hot } from 'jasmine-marbles';
import { LoadCollectionFailure, LoadCollectionRequest, LoadCollectionSucess } from '../actions/collection.actions';
import { Book } from '@domain/catalog/item.model';
import { BooksService } from '../services/books.service';

describe('SearchEffects', () => {
  let actions$: Observable<any>;
  let effects: CollectionEffects;
  let booksService: BooksService;
  const books = [new Book('123', 12), new Book('456', 35)];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        CollectionEffects,
        BooksService,
        provideMockActions(() => actions$),
      ]
    });

    booksService = TestBed.get(BooksService);

    effects = TestBed.get(CollectionEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should call BooksService and dispatch LoadCollectionSucess when LoadCollectionRequest was dispatched', () => {
    spyOn(booksService, 'getBooks').and.returnValue(of(books));
    const action = new LoadCollectionRequest();
    const completion = new LoadCollectionSucess({books});

    actions$ = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.load$).toBeObservable(expected);
    expect(booksService.getBooks).toHaveBeenCalled();
  });

  it('should dispatch LoadCollectionError when BooksService is down', () => {
    const error = new Error('Ouch Charlie!');
    spyOn(booksService, 'getBooks').and.returnValue(throwError(error));

    const action = new LoadCollectionRequest();
    const completion = new LoadCollectionFailure({error});

    actions$ = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.load$).toBeObservable(expected);
    expect(booksService.getBooks).toHaveBeenCalled();
  });

});
