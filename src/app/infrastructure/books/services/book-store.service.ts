import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs/index';
import { filter, switchMap, take, tap } from 'rxjs/operators';
import { LoadCollectionRequest } from '../actions/collection.actions';
import { ExternalBookInterface } from '@application/product/external-book.interface';
import * as fromBooks from '../reducers/index';

@Injectable({
  providedIn: 'root'
})
export class BookStoreService {

  constructor(private store: Store<fromBooks.State>) { }

  public loadCollectionIfNotLoaded$(): Observable<{ [id: string]: ExternalBookInterface; }> {
    return this.store
      .pipe(
        select(fromBooks.getCollectionLoadingState),
        tap((loadingState) => {
          if (!loadingState.loaded && !loadingState.loading) {
            this.store.dispatch(new LoadCollectionRequest());
          }
        }),
        filter((loadingState) => loadingState.loaded && !loadingState.loading),
        switchMap(() => this.store.select(fromBooks.getCollectionEntities)),
        take(1)
      );
  }
}
