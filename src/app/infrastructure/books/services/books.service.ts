import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '@env/environment';
import { ExternalBookInterface } from '@application/product/external-book.interface';
import { BooksServiceInterface } from '@application/product/books.service';

@Injectable({
  providedIn: 'root'
})
export class BooksService implements BooksServiceInterface {

  private readonly booksApi = `${environment.apiPath}/books`;

  constructor(private http: HttpClient) { }

  getBooks(): Observable<ExternalBookInterface[]> {
    return this.http.get<ExternalBookInterface[]>(this.booksApi);
  }

  search(books: ExternalBookInterface[], query: string): Observable<ExternalBookInterface[]> {
    return of(books.filter(book => {
      return book.title.toLowerCase().includes(query.toLowerCase())
        || book.synopsis.find(paragraph => paragraph.toLowerCase().includes(query.toLowerCase()));
    }));
  }
}
