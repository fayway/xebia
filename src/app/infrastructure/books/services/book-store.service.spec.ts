import { TestBed } from '@angular/core/testing';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { ExternalBookInterface } from '@application/product/external-book.interface';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoadCollectionRequest } from '../actions/collection.actions';
import { of } from 'rxjs';
import { concatMap, take, tap } from 'rxjs/operators';
import { reducers, State, getFlatCollectionEntities } from '../reducers/index';
import { BookStoreService } from './book-store.service';
import { CollectionEffects } from '../effects/collection-effects.service';
import { BooksService } from '@infrastructure/books/services/books.service';

describe('BookStoreService', () => {
  let service: BookStoreService;
  let booksService: BooksService;
  let store: Store<State>;
  const book1 = <ExternalBookInterface>{isbn: '123', price: 25, title: 'Book 123', cover: 'http://cover1.com', synopsis: ['Lorem ipsum']};
  const book2 = <ExternalBookInterface>{isbn: '456', price: 30, title: 'Book 456', cover: 'http://cover.com', synopsis: ['Lorem ipsum']};
  const books = [book1, book2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BooksService,
        BookStoreService
      ],
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot({
          'books': combineReducers(reducers)
        }),
        EffectsModule.forRoot([CollectionEffects]),
      ]
    });

    service = TestBed.get(BookStoreService);
    booksService = TestBed.get(BooksService);
    store = TestBed.get(Store);

    spyOn(booksService, 'getBooks').and.returnValue(of(books));
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should dispatch LoadCollectionRequest to preload empty store and then return preloaded entities', (done: DoneFn) => {
    const expectedEntities = {
      [book1.isbn]: book1,
      [book2.isbn]: book2,
    };
    service.loadCollectionIfNotLoaded$()
      .pipe(
        take(1),
        tap((result) => {
          expect(store.dispatch).toHaveBeenCalledWith(new LoadCollectionRequest());
          expect(result).toEqual(expectedEntities);
        }),
        concatMap(() => store.select(getFlatCollectionEntities)),
        tap((flatBooks) => {
          expect(flatBooks).toEqual(books);
        })
      ).subscribe(() => done());
  });
});
