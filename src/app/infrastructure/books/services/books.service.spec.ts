import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { environment } from '@env/environment';
import { BooksService } from './books.service';
import { ExternalBookInterface } from '@application/product/external-book.interface';

describe('BooksService', () => {
  let service: BooksService;
  let http: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: HttpClient, useValue: {get: jest.fn()}},
        BooksService
      ]
    });

    service = TestBed.get(BooksService);
    http = TestBed.get(HttpClient);
  });

  const books: ExternalBookInterface[] = [{
    'isbn': 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
    'title': 'Henri Potier à l\'école des sorciers',
    'price': 35,
    'cover': 'http://henri-potier.xebia.fr/hp0.jpg',
    'synopsis': ['Lorem ipsum dolor sit amet', 'Consectetur adipiscing elit']
  }, {
      'isbn': 'a460afed-e5e7-4e39-a39d-c885c05db861',
      'title': 'Henri Potier et la Chambre des secrets',
      'price': 30,
      'cover': 'http://henri-potier.xebia.fr/hp1.jpg',
      'synopsis': ['Lorem ipsum dolor sit amet']
    }];

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getBooks() should retrieve available books', async () => {
    http.get = jest.fn(() => of(books));
    const data = await service.getBooks().toPromise();
    expect(data).toEqual(books);
    expect(http.get).toHaveBeenCalledWith(`${environment.apiPath}/books`);
  });

  it ('search()', async () => {
    let data = await service.search(books, 'potier').toPromise();
    expect(data).toEqual(books);

    data = await service.search(books, 'lorem').toPromise();
    expect(data).toEqual(books);

    data = await service.search(books, 'chambre').toPromise();
    expect(data.length).toEqual(1);

    data = await service.search(books, 'Consectetur').toPromise();
    expect(data.length).toEqual(1);
    expect(data[0]).toEqual(books[0]);
  });
});
