import { TestBed } from '@angular/core/testing';
import { CollectionPreloadGuard } from './collection-preload.guard';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import * as fromBooks from '../reducers/index';
import { RouterTestingModule } from '@angular/router/testing';
import { BookStoreService } from '../services/book-store.service';
import { take } from 'rxjs/operators';
import { LoadCollectionRequest } from '../actions/collection.actions';

describe('CollectionPreloadGuard', () => {
  let store: Store<fromBooks.State>;
  let guard: CollectionPreloadGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          'books': combineReducers(fromBooks.reducers)
        }, {
          initialState: <fromBooks.State>{
            books: {
              collection: {
                ids: [],
                entities: {},
                loading: false,
                loaded: false,
                error: null
              }
            }
          }
        }),
        RouterTestingModule
      ],
      providers: [CollectionPreloadGuard, BookStoreService]
    });

    store = TestBed.get(Store);
    guard = TestBed.get(CollectionPreloadGuard);

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should ...', () => {
    expect(guard).toBeTruthy();
  });

  it('should dispatch LoadCollectionRequest() if store was not preloaded', () => {
    guard.canActivate(null, null)
      .pipe(take(1))
      .subscribe((canActivate) => {
        expect(canActivate).toBe(false);
        expect(store.dispatch).toHaveBeenCalledWith(new LoadCollectionRequest());
      });
  });
});
