import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BookStoreService } from '../services/book-store.service';

@Injectable({
  providedIn: 'root'
})
export class BookExistenceGuard implements CanActivate {
  constructor(private bookStoreService: BookStoreService, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.bookStoreService
      .loadCollectionIfNotLoaded$()
      .pipe(
        map(entities => {
          if (!entities[route.params['isbn']]) {
            this.router.navigate(['/404']);
            return false;
          }
          return true;
        })
      );
  }
}
