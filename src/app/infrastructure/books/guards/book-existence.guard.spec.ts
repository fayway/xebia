import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { take } from 'rxjs/operators';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { ExternalBookInterface } from '@application/product/external-book.interface';
import * as fromBooks from '../reducers/index';
import { BookExistenceGuard } from './book-existence.guard';

describe('BookExistenceGuard', () => {
  let store: Store<fromBooks.State>;
  let router: Router;
  let guard: BookExistenceGuard;
  const book1 = <ExternalBookInterface>{isbn: '123', price: 25, title: 'Book 123', cover: 'http://cover1.com', synopsis: ['Lorem ipsum']};
  const book2 = <ExternalBookInterface>{isbn: '456', price: 30, title: 'Book 456', cover: 'http://cover.com', synopsis: ['Lorem ipsum']};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          'books': combineReducers(fromBooks.reducers)
        }, {
          initialState: <fromBooks.State>{
            books: {
              collection: {
                ids: [book1.isbn, book2.isbn],
                entities: {
                  [book1.isbn]: book1,
                  [book2.isbn]: book2,
                },
                loading: false,
                loaded: true,
                error: null
              }
            }
          }
        }),
        RouterTestingModule
      ],
      providers: [
        BookExistenceGuard
      ]
    });

    store = TestBed.get(Store);
    router = TestBed.get(Router);
    guard = TestBed.get(BookExistenceGuard);

    spyOn(router, 'navigate').and.stub();
  });

  it('should ...', () => {
    expect(guard).toBeTruthy();
  });

  it('should activate route when book exists', () => {
    const route = new ActivatedRouteSnapshot();
    route.params = {isbn: book1.isbn};

    guard.canActivate(route, null)
      .pipe(take(1))
      .subscribe((canActivate) => {
        expect(canActivate).toBe(true);
        expect(router.navigate).not.toHaveBeenCalledWith(['/404']);
      });
  });

  it('should redirect to /404 when book does not exist', () => {
    const route = new ActivatedRouteSnapshot();
    route.params = {isbn: 'ouch'};

    guard.canActivate(route, null)
      .pipe(take(1))
      .subscribe((canActivate) => {
        expect(canActivate).toBe(false);
        expect(router.navigate).toHaveBeenCalledWith(['/404']);
      });
  });
});
