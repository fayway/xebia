import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { BookStoreService } from '../services/book-store.service';

@Injectable({
  providedIn: 'root'
})
export class CollectionPreloadGuard implements CanActivate {
  constructor(private bookStoreService: BookStoreService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.bookStoreService
      .loadCollectionIfNotLoaded$()
      .pipe(
        switchMap(() => of(true)),
        catchError(() => of(false))
      );
  }
}
