import { Action } from '@ngrx/store';
import { ExternalBookInterface } from '@application/product/external-book.interface';

export enum CollectionActionTypes {
  LoadCollectionRequest = '[Books] Load Collection Request',
  LoadCollectionSuccess = '[Books] Load Collection Success',
  LoadCollectionFailure = '[Books] Load Collection Failure',
}

export class LoadCollectionRequest implements Action {
  readonly type = CollectionActionTypes.LoadCollectionRequest;
}

export class LoadCollectionSucess implements Action {
  readonly type = CollectionActionTypes.LoadCollectionSuccess;
  constructor(public payload: { books: ExternalBookInterface[] }) {}
}

export class LoadCollectionFailure implements Action {
  readonly type = CollectionActionTypes.LoadCollectionFailure;
  constructor(public payload: { error: Error }) {}
}

export type CollectionActions =
  LoadCollectionRequest |
  LoadCollectionSucess |
  LoadCollectionFailure;
