import { Action } from '@ngrx/store';
import { ExternalBookInterface } from '@application/product/external-book.interface';

export enum SearchActionTypes {
  SearchCollectionRequest = '[Books] Search Collection Request',
  SearchCollectionSuccess = '[Books] Search Collection Success',
  SearchCollectionFailure = '[Books] Search Collection Failure',
}

export class SearchRequest implements Action {
  readonly type = SearchActionTypes.SearchCollectionRequest;
  constructor(public payload: { query: string }) {}
}

export class SearchSucess implements Action {
  readonly type = SearchActionTypes.SearchCollectionSuccess;
  constructor(public payload: { books: ExternalBookInterface[] }) {}
}

export class SearchFailure implements Action {
  readonly type = SearchActionTypes.SearchCollectionFailure;
  constructor(public payload: { error: Error }) {}
}

export type SearchActions =
  SearchRequest |
  SearchSucess |
  SearchFailure;
