import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { BooksListPageComponent } from './books-list-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HighlightPipe } from '../../../shared/pipes/highlight.pipe';
import * as fromBooks from '../../reducers/index';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { Router } from '@angular/router';
import { SearchRequest } from '../../actions/search.actions';
import { BookComponent } from '@infrastructure/shared/components/book/book.component';
import { MaterialModule } from '@infrastructure/shared/material/material.module';
import { Book } from '@domain/catalog/item.model';
import { AddProductToCart } from '@infrastructure/shop/actions/cart.actions';

describe('BooksListPageComponent', () => {
  let component: BooksListPageComponent;
  let fixture: ComponentFixture<BooksListPageComponent>;
  let store: Store<fromBooks.State>;
  let router: Router;
  const book1 = new Book('123', 25, 'Book 123', 'http://cover1.com', ['Lorem ipsum']);
  const book2 = new Book('456', 30, 'Book 456', 'http://cover.com', ['Lorem ipsum']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BooksListPageComponent, BookComponent, HighlightPipe],
      imports: [
        MaterialModule,
        RouterTestingModule,
        NoopAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({
          'books': combineReducers(fromBooks.reducers)
          }, {
          initialState: <fromBooks.State>{
            books: {
              collection: {
                ids: [book1.isbn, book2.isbn],
                entities: {
                  [book1.isbn]: book1,
                  [book2.isbn]: book2,
                },
                loading: false,
                loaded: true,
                error: null
              }
            }
          }
        })
      ]
    });

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();

    router = TestBed.get(Router);
    spyOn(router, 'navigate').and.callThrough();

    fixture = TestBed.createComponent(BooksListPageComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(fixture).toMatchSnapshot();
  });

  it('should dispatch SearchRequest Action on search input value changes', (done: DoneFn) => {
    fixture.detectChanges();
    component.searchForm.get('query').setValue('123');
    // delay assertion because of debounceTime(300) on query field valueChanges obs
    // TODO use Test Scheduler
    setTimeout(() => {
      expect(store.dispatch).toHaveBeenCalledWith(new SearchRequest({query: '123'}));
      done();
    }, 350);
  });

  it('should dispatch AddProductToCart Action on the right button click', () => {
    fixture.detectChanges();
    const hostElement = fixture.nativeElement;
    //
    const book1Add: HTMLButtonElement = hostElement.querySelector(`button[test-id="add:${book1.isbn}"]`);
    book1Add.click();
    expect(store.dispatch).toHaveBeenCalledWith(new AddProductToCart({product: book1}));
    //
    const book2Add: HTMLButtonElement = hostElement.querySelector(`button[test-id="add:${book2.isbn}"]`);
    book2Add.click();
    expect(store.dispatch).toHaveBeenCalledWith(new AddProductToCart({product: book2}) );
  });

  it('should navigate to the right book detail route', () => {
    fixture.detectChanges();
    const hostElement = fixture.nativeElement;
    //
    const book1Detail: HTMLButtonElement = hostElement.querySelector(
      `button[test-id="detail:${book1.isbn}"]`
    );
    book1Detail.click();
    expect(router.navigate).toHaveBeenCalledWith(['/book/', book1.isbn]);
    //
    const book2Detail: HTMLButtonElement = hostElement.querySelector(
      `button[test-id="detail:${book2.isbn}"]`
    );
    book2Detail.click();
    expect(router.navigate).toHaveBeenCalledWith(['/book/', book2.isbn]);
  });
});
