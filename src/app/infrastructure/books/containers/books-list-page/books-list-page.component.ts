import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromBooks from '../../reducers/index';
import { mapBooksToDomain } from '../../reducers/index';
import { SearchRequest } from '../../actions/search.actions';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { Book } from '@domain/catalog/item.model';
import { AddProductToCart } from '@infrastructure/shop/actions/cart.actions';

@Component({
  selector: 'app-books-list-page',
  templateUrl: './books-list-page.component.html',
  styleUrls: ['./books-list-page.component.scss'],
})
export class BooksListPageComponent implements OnInit, OnDestroy {

  books$: Observable<Book[]>;
  query$: Observable<string>;
  loading$: Observable<boolean>;

  searchForm: FormGroup;
  subs = new Subscription();

  constructor(
    private store: Store<fromBooks.State>,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.searchForm = this.fb.group({
      query: ''
    });
  }

  ngOnInit() {
    this.books$ = this.store.pipe(
      select(fromBooks.getFiltredBooks),
      map(books => mapBooksToDomain(books))
    );
    this.query$ = this.store.select(fromBooks.getSearchQuery);
    this.loading$ = this.store.select(fromBooks.isLoading);

    this.subs.add(
      this.query$
        .subscribe(query => this.searchForm
          .get('query').
          setValue(query)
        )
    );

    this.subs.add(
      this.searchForm
        .get('query')
        .valueChanges
        .pipe(
          debounceTime(300),
          distinctUntilChanged(),
        )
        .subscribe((query) => this.store.dispatch(new SearchRequest({query})))
    );
  }

  addBookToCart(book: Book) {
    this.store.dispatch(new AddProductToCart({product: book}));
  }

  goToBookDetail(book: Book) {
    this.router.navigate(['/book/', book.isbn]);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
