import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import * as fromBooks from '../../reducers/index';
import { State } from '../../reducers/index';
import { Book } from '@domain/catalog/item.model';
import { AddProductToCart } from '@infrastructure/shop/actions/cart.actions';

@Component({
  selector: 'app-book-detail-page',
  templateUrl: './book-detail-page.component.html',
  styleUrls: ['./book-detail-page.component.scss']
})
export class BookDetailPageComponent implements OnInit {

  book$: Observable<Book>;
  query$: Observable<string>;

  constructor(private store: Store<State>, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.book$ = this.route.params
      .pipe(
        map(params => params['isbn']),
        switchMap((isbn) => this.store.select(fromBooks.getBook(isbn))),
        map(book => fromBooks.mapBookToDomain(book))
      );
    this.query$ = this.store.select(fromBooks.getSearchQuery);
  }

  goToBooksList() {
    this.router.navigate(['/']);
  }

  addBookToCart(book: Book) {
    this.store.dispatch(new AddProductToCart({product: book}));
  }
}
