import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { BookDetailPageComponent } from './book-detail-page.component';
import { ExternalBookInterface } from '@application/product/external-book.interface';
import * as fromBooks from '../../reducers/index';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { BookComponent } from '@infrastructure/shared/components/book/book.component';
import { HighlightPipe } from '@infrastructure/shared/pipes/highlight.pipe';
import { MaterialModule } from '@infrastructure/shared/material/material.module';
import { AddProductToCart } from '@infrastructure/shop/actions/cart.actions';
import { Book } from '@domain/catalog/item.model';


describe('BookDetailPageComponent', () => {
  let component: BookDetailPageComponent;
  let fixture: ComponentFixture<BookDetailPageComponent>;
  let store: Store<fromBooks.State>;
  let router: Router;
  const book1 = new Book('123', 25, 'Book 123', 'http://cover1.com', ['Lorem ipsum']);
  const book2 = new Book('456', 30, 'Book 456', 'http://cover.com', ['Lorem ipsum']);

  const params = new BehaviorSubject({isbn: book1.isbn});

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BookDetailPageComponent, BookComponent, HighlightPipe],
      imports: [
        MaterialModule,
        RouterTestingModule,
        StoreModule.forRoot({
          'books': combineReducers(fromBooks.reducers)
        }, {
          initialState: <fromBooks.State>{
            books: {
              collection: {
                ids: [book1.isbn, book2.isbn],
                entities: {
                  [book1.isbn]: book1,
                  [book2.isbn]: book2,
                },
                loading: false,
                loaded: true,
                error: null
              }
            }
          }
        })
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { params },
        },
      ]
    });

    store = TestBed.get(Store);
    router = TestBed.get(Router);
    spyOn(router, 'navigate').and.callThrough();
    spyOn(store, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(BookDetailPageComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(fixture).toMatchSnapshot();
  });

  it('should select book by route isbn param', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(fixture).toMatchSnapshot();
    });
  }));

  it('goToBooksList()', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const hostElement = fixture.nativeElement;
    //
    fixture.whenStable().then(() => {
      const returnBack: HTMLButtonElement = hostElement.querySelector(
        `button[test-id="return:${book1.isbn}"]`
      );
      returnBack.click();
      expect(router.navigate).toHaveBeenCalledWith(['/']);
    });
    //
  }));

  it('addBookToCart()', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const hostElement = fixture.nativeElement;
    //
    fixture.whenStable().then(() => {
      const returnBack: HTMLButtonElement = hostElement.querySelector(
        `button[test-id="add:${book1.isbn}"]`
      );
      returnBack.click();
      expect(store.dispatch).toHaveBeenCalledWith(new AddProductToCart({product: book1}));
    });
    //
  }));
});
