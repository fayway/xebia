import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './shell/components/navbar/navbar.component';
import { ShellComponent } from './shell/containers/shell.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NotfoundPageComponent } from './shell/pages/404/notfound-page/notfound-page.component';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressRouterModule } from '@ngx-progressbar/router';
import { SharedModule } from '@infrastructure/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    NgProgressModule.forRoot(),
    NgProgressRouterModule,
    SharedModule
  ],
  declarations: [
    ShellComponent,
    NavbarComponent,
    NotfoundPageComponent
  ]
})
export class CoreModule { }
