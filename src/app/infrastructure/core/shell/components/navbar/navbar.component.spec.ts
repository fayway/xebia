import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NavbarComponent } from './navbar.component';
import { MaterialModule } from '@infrastructure/shared/material/material.module';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [MaterialModule, RouterTestingModule]
    });

    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should display cart items count', () => {
    component.cartCount = 3;
    fixture.detectChanges();
    const bookElement = fixture.nativeElement;
    const cartCountBadge: HTMLElement = bookElement.querySelector(`.mat-badge-content`);
    expect(cartCountBadge.textContent).toContain(3);
  });
});
