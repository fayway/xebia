import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getCartProductsCount, State } from '@infrastructure/shop/reducers/index';

@Component({
  selector: 'app-root',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {

  cartCount$: Observable<number>;

  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.cartCount$ = this.store.select(getCartProductsCount);
  }
}
