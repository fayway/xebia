import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShellComponent } from './shell.component';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgProgressModule } from '@ngx-progressbar/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject } from 'rxjs';
import { MaterialModule } from '@infrastructure/shared/material/material.module';
import { getCartProductsCount } from '@infrastructure/shop/reducers';

describe('ShellComponent', () => {
  let component: ShellComponent;
  let fixture: ComponentFixture<ShellComponent>;
  let store: Store;
  const count$ = new BehaviorSubject(3);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShellComponent, NavbarComponent ],
      imports: [ RouterTestingModule, MaterialModule, NgProgressModule.forRoot() ],
      providers: [
        {
          provide: Store,
          useValue: {
            select: jest.fn(() => count$)
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShellComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
  });

  it('should create and render initial Store state', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(store.select).toHaveBeenCalledWith(getCartProductsCount);
    expect(fixture).toMatchSnapshot();
  });

  it('should render Store changes correctly', () => {
    count$.next(4);
    fixture.detectChanges();
    expect(fixture).toMatchSnapshot();
  });
});
