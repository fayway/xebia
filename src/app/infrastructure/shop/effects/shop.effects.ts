import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';

import { CartActionTypes } from '../actions/cart.actions';
import { CommercialOffersFailure, CommercialOffersRequest, CommercialOffersSuccess, OffersActionTypes } from '../actions/offers.actions';
import * as fromCart from '../reducers/index';
import { HttpErrorResponse } from '@angular/common/http';
import { OffersService } from '../services/offers.service';

@Injectable()
export class ShopEffects {

  @Effect()
  offersRequest$ = this.actions$.ofType(
    CartActionTypes.AddProduct,
    CartActionTypes.RemoveProduct,
    CartActionTypes.IncrementQuantity,
    CartActionTypes.DecrementQuantity
  ).pipe(
    withLatestFrom(this.store.select(fromCart.getCartProducts)),
    map(([action, cartProducts]) => new CommercialOffersRequest({cartProducts}))
  );

  @Effect()
  offers$ = this.actions$.ofType(
    OffersActionTypes.CommercialOffersRequest
  ).pipe(
    map((action: CommercialOffersRequest) => action.payload.cartProducts),
    switchMap((cartProducts) => {
      if (!cartProducts.length) {
        return of(new CommercialOffersSuccess({commercialOffers: null}));
      }
      return this.offersService.getCommercialOffers(cartProducts)
        .pipe(
          map((commercialOffers) => new CommercialOffersSuccess({commercialOffers})),
          catchError((response: HttpErrorResponse) => of(new CommercialOffersFailure({error: response})))
        );
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<fromCart.State>,
    private offersService: OffersService
    ) {}
}
