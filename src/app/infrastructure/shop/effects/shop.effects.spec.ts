import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of, throwError } from 'rxjs';
import { ShopEffects } from './shop.effects';
import { Store } from '@ngrx/store';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Actions } from '@ngrx/effects';
import { cold, hot } from 'jasmine-marbles';
import { CommercialOffersFailure, CommercialOffersRequest, CommercialOffersSuccess } from '../actions/offers.actions';
import { PercentageOffer } from '@domain/promotion/percentage-offer.model';
import { MinusOffer } from '@domain/promotion/minus-offer.model';
import { CommercialOffers } from '@domain/promotion/offer.model';
import { Book } from '@domain/catalog/item.model';
import { CartProductInterface } from '@domain/cart/cart-product.model';
import { OffersService } from '../services/offers.service';

describe('CartService', () => {
  let actions$: Observable<any>;
  let effects: ShopEffects;
  let offersService: OffersService;
  //
  const book1 = new Book('123', 25, 'Book 123');
  const book2 = new Book('456', 30, 'Book 456');
  const cartProducts: CartProductInterface[] = [
    {product: book1, quantity: 1},
    {product: book2, quantity: 2},
  ];
  //
  const percentageOffer = new PercentageOffer(5);
  const minusOffer = new MinusOffer(15);
  const commercialOffers = new CommercialOffers([
    percentageOffer,
    minusOffer
  ]);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        ShopEffects,
        OffersService,
        {
          provide: Store,
          useValue: {
            select: () => of(null)
          }
        },
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(ShopEffects);
    actions$ = TestBed.get(Actions);
    offersService = TestBed.get(OffersService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should dispatch CommercialOffersSuccess with returned Offers after a CommercialOffersRequest', () => {
    spyOn(offersService, 'getCommercialOffers').and.returnValue(of(commercialOffers));

    const action = new CommercialOffersRequest({cartProducts});
    const completion = new CommercialOffersSuccess({commercialOffers});

    actions$ = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.offers$).toBeObservable(expected);
    expect(offersService.getCommercialOffers).toHaveBeenCalledWith(cartProducts);
  });

  it('should dispatch CommercialOffersFailure when offersService is down', () => {
    const error = new Error('Ouch Charlie!');
    spyOn(offersService, 'getCommercialOffers').and.returnValue(throwError(error));

    const action = new CommercialOffersRequest({cartProducts});
    const completion = new CommercialOffersFailure({error});

    actions$ = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.offers$).toBeObservable(expected);
    expect(offersService.getCommercialOffers).toHaveBeenCalled();
  });
});
