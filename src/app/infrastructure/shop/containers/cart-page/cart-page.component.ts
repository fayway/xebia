import { Component, OnDestroy, OnInit } from '@angular/core';
import { Book } from '@domain/catalog/item.model';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import { DecrementProductQuantity, IncrementProductQuantity, RemoveProductFromCart } from '../../actions/cart.actions';
import * as fromCart from '../../reducers/index';
import { CartProduct } from '@domain/cart/cart-product.model';
import { Cart } from '@domain/cart/cart.model';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent implements OnInit, OnDestroy {

  cartProducts$: Observable<CartProduct[]>;
  cart$: Observable<Cart>;
  loading: boolean;

  subs = new Subscription();
  constructor(private store: Store<fromCart.State>) { }

  ngOnInit() {
    this.cartProducts$ = this.store.pipe(
      select(fromCart.getCartProducts),
      map(cartProducts => fromCart.mapToDomainCartProducts(cartProducts))
    );

    this.cart$ = this.store
      .pipe(
        select(fromCart.getCommercialOffers),
        withLatestFrom(this.cartProducts$),
        map(([commercialOffers, cartProducts]) => new Cart(cartProducts, commercialOffers))
      );

    this.subs.add(
      this.store
        .select(fromCart.getCommercialOffersLoading)
        .subscribe((loading) => this.loading = loading)
    );
  }

  removeItem(book: Book) {
    this.store.dispatch(new RemoveProductFromCart({product: book}));
  }

  incerementBookCartQuantity(book: Book) {
    this.store.dispatch(new IncrementProductQuantity({product: book}));
  }

  decrementBookCartQuantity(book: Book) {
    this.store.dispatch(new DecrementProductQuantity({product: book}));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
