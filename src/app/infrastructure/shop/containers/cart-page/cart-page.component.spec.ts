import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from '@infrastructure/shared/material/material.module';
import { BookComponent } from '@infrastructure/shared/components/book/book.component';
import { CartPageComponent } from './cart-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HighlightPipe } from '@infrastructure/shared/pipes/highlight.pipe';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { CartProductInterface } from '@domain/cart/cart-product.model';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import * as fromShop from '../../reducers/index';
import { Book } from '@domain/catalog/item.model';
import { DecrementProductQuantity, IncrementProductQuantity, RemoveProductFromCart } from '../../actions/cart.actions';

describe('CartPageComponent', () => {
  let component: CartPageComponent;
  let fixture: ComponentFixture<CartPageComponent>;
  let store: Store<fromShop.State>;

  const book1 = new Book('123', 25.36, 'Book 123', 'http://cover1.com', ['Lorem ipsum']);
  const book2 = new Book('456', 99.99, 'Book 456', 'http://cover1.com', ['Lorem ipsum']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CartPageComponent, BookComponent, HighlightPipe],
      imports: [
        MaterialModule,
        HttpClientTestingModule,
        RouterTestingModule,
        StoreModule.forRoot({
          'shop': combineReducers(fromShop.reducers)
        }, {
          initialState: <fromShop.State>{
            shop: {
              cart: {
                cartProducts: <CartProductInterface[]>[
                  {product: book1, quantity: 1},
                  {product: book2, quantity: 2},
                ]
              }
            }
          }
        }),
      ]
    });

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(CartPageComponent);
    component = fixture.componentInstance;
  }));

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(fixture).toMatchSnapshot();
  });

  it('should dispatch IncrementProductQuantity Action on Increment button click', () => {
    fixture.detectChanges();
    const bookElement = fixture.nativeElement;
    //
    const incBtn: HTMLButtonElement = bookElement.querySelector(`button[test-id="incr:${book2.getId()}"]`);
    incBtn.click();
    expect(store.dispatch).toHaveBeenCalledWith(new IncrementProductQuantity({product: book2}));
  });

  it('should dispatch DecrementProductQuantity Action on Decrement button click', () => {
    fixture.detectChanges();
    const bookElement = fixture.nativeElement;
    //
    const incBtn: HTMLButtonElement = bookElement.querySelector(`button[test-id="decr:${book1.getId()}"]`);
    incBtn.click();
    expect(store.dispatch).toHaveBeenCalledWith(new DecrementProductQuantity({product: book1}));
  });

  it('should dispatch RemoveProductFromCart Action on Remove button click', () => {
    fixture.detectChanges();
    const bookElement = fixture.nativeElement;
    //
    const incBtn: HTMLButtonElement = bookElement.querySelector(`button[test-id="rm:${book2.getId()}"]`);
    incBtn.click();
    expect(store.dispatch).toHaveBeenCalledWith(new RemoveProductFromCart({product: book2}));
  });
});
