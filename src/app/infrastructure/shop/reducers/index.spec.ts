import { Book } from '@domain/catalog/item.model';
import { CartProductInterface } from '@domain/cart/cart-product.model';
import { getCartProductsCount, getCartTotal } from './index';

describe('Books Selectors', () => {
  const book1 = new Book('123', 12.01, 'Book 123');
  const book2 = new Book('456', 36.02, 'Book 456');
  const cartProducts: CartProductInterface[] = [
    {product: book1, quantity: 2},
    {product: book2, quantity: 3},
  ];

  it('getCartProductsCount should return items count', function () {
    expect(getCartProductsCount.projector(cartProducts)).toBe(5);
  });

  it('getCartTotal should select the correct book by its id', function () {
    expect(getCartTotal.projector(cartProducts)).toBe(
      (cartProducts[0].product.price * cartProducts[0].quantity) + (cartProducts[1].product.price * cartProducts[1].quantity)
    );
  });

});
