import { CartActions, CartActionTypes } from '../actions/cart.actions';
import { CartProductInterface } from '@domain/cart/cart-product.model';
import { Cart } from '@domain/cart/cart.model';

export interface State {
  cartProducts: CartProductInterface[];
}

export const initialState: State = {
  cartProducts: [
    // {product: new Book('c8fabf68-8374-48fe-a7ea-a00ccd07afff', 35), quantity: 2}
  ]
};

export function reducer(state = initialState, action: CartActions): State {
  switch (action.type) {
    case CartActionTypes.AddProduct:
    case CartActionTypes.IncrementQuantity:
      return {
        ...state,
        cartProducts: Cart.incrementProduct(state.cartProducts, action.payload.product)
      };

    case CartActionTypes.DecrementQuantity:
      return {
        ...state,
        cartProducts: Cart.decrementProduct(state.cartProducts, action.payload.product)
      };

    case CartActionTypes.RemoveProduct:
      return {
        ...state,
        cartProducts: Cart.removeProduct(state.cartProducts, action.payload.product)
      };

    default:
      return state;
  }
}
