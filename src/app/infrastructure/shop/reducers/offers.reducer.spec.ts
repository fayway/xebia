import { initialState, reducer } from './offers.reducer';
import { Book } from '@domain/catalog/item.model';
import { CommercialOffersFailure, CommercialOffersRequest, CommercialOffersSuccess } from '../actions/offers.actions';
import { CartProductInterface } from '@domain/cart/cart-product.model';
import { PercentageOffer } from '@domain/promotion/percentage-offer.model';
import { MinusOffer } from '@domain/promotion/minus-offer.model';
import { CommercialOffers } from '@domain/promotion/offer.model';

describe('Offers Reducer', () => {
  const book1 = new Book('123', 35);
  const book2 = new Book('456', 23);
  const cartProducts: CartProductInterface[] = [
    {product: book1, quantity: 2},
    {product: book2, quantity: 1},
  ];

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('CommercialOffersRequest action', () => {
    it('should return the state in request state', () => {
      const action = new CommercialOffersRequest({cartProducts});
      const result = reducer(initialState, action);
      expect(result).toMatchSnapshot();

    });
  });

  describe('CommercialOffersSuccess action', () => {
    it('should return the state with Commercial Offers', () => {
      const percentageOffer = new PercentageOffer(5);
      const minusOffer = new MinusOffer(15);

      const commercialOffers = new CommercialOffers([
        percentageOffer,
        minusOffer,
      ]);
      const action = new CommercialOffersSuccess({commercialOffers});
      const result = reducer(initialState, action);
      expect(result).toMatchSnapshot();

    });
  });

  describe('CommercialOffersError action', () => {
    it('should return the state in the error state', () => {
      const action = new CommercialOffersFailure({error: new Error('Ouch Charlie')});
      const result = reducer(initialState, action);
      expect(result).toMatchSnapshot();
    });
  });
});
