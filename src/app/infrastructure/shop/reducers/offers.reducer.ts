import { OffersActions, OffersActionTypes } from '../actions/offers.actions';
import { CommercialOffers } from '@domain/promotion/offer.model';

export interface State {
  commercialOffers: CommercialOffers;
  loading: boolean;
  loaded: boolean;
  error: Error;
}

export const initialState: State = {
  commercialOffers: null,
  loading: false,
  loaded: false,
  error: null,
};

export function reducer(state = initialState, action: OffersActions): State {
  switch (action.type) {
    case OffersActionTypes.CommercialOffersRequest:
      return {
        ...state,
        commercialOffers: null,
        loading: true,
        loaded: false,
        error: null
      };

    case OffersActionTypes.CommercialOffersSuccess:
      return {
        ...state,
        commercialOffers: action.payload.commercialOffers,
        loading: false,
        loaded: true,
        error: null
      };

    case OffersActionTypes.CommercialOffersFailure:
      return {
        ...state,
        commercialOffers: null,
        loading: false,
        loaded: false,
        error: action.payload.error
      };

    default:
      return state;
  }
}
