import { initialState, reducer } from './cart.reducer';
import { AddProductToCart, DecrementProductQuantity, IncrementProductQuantity } from '../actions/cart.actions';
import { Book } from '@domain/catalog/item.model';

describe('Cart Reducer', () => {
  const book1 = new Book('c8fabf68-8374-48fe-a7ea-a00ccd07afff', 35);

  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('AddProductToCart action', () => {
    it('should return the state with added product', () => {
      const action = new AddProductToCart({product: book1});
      const result = reducer(initialState, action);
      expect(result).toMatchSnapshot();

    });
  });

  describe('IncrementProductQuantity action', () => {
    it('should return the state with incremented product qte', () => {
      const action = new IncrementProductQuantity({product: book1});
      const result = reducer({
          ...initialState,
          cartProducts: [
            {product: book1, quantity: 1}
          ]
        }, action);
      expect(result).toMatchSnapshot();

    });
  });

  describe('DecrementProductQuantity action', () => {
    it('should return the state with incremented product qte', () => {
      const action = new DecrementProductQuantity({product: book1});
      const result = reducer(
        {
          ...initialState,
          cartProducts: [
            {product: book1, quantity: 3}
          ]
        },
        action);
      expect(result).toMatchSnapshot();

    });
  });
});
