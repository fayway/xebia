import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../reducers/index';
import * as fromCart from './cart.reducer';
import * as fromOffers from './offers.reducer';
import { CartProduct } from '@domain/cart/cart-product.model';

export interface CartState {
  cart: fromCart.State;
  offers: fromOffers.State;
}

export interface State extends fromRoot.State {
  shop: CartState;
}

export const reducers: ActionReducerMap<CartState> = {
  cart: fromCart.reducer,
  offers: fromOffers.reducer,
};

export const getShopFeature = createFeatureSelector<CartState>('shop');

export const getCartState = createSelector(
  getShopFeature,
  state => state.cart
);

export const getCartProducts = createSelector(
  getCartState,
  state => state.cartProducts
);

export const getCommercialOffersState = createSelector(
  getShopFeature,
  state => state.offers
);

export const getCommercialOffers = createSelector(
  getCommercialOffersState,
  offres => offres.commercialOffers
);

export const getCommercialOffersLoading = createSelector(
  getCommercialOffersState,
  offres => offres.loading
);

export const mapCartProductToDomain = cartProduct => CartProduct.adapt(cartProduct);
export const mapToDomainCartProducts = cartProducts => cartProducts.map(cartProduct => mapCartProductToDomain(cartProduct));

export const getCartProductsCount = createSelector(
  getCartProducts,
  cartProducts => cartProducts.reduce((acc, cartProduct) => acc + cartProduct.quantity, 0)
);

export const getCartTotal = createSelector(
  getCartProducts,
  cartProducts => cartProducts.reduce((acc, cartProduct) => acc + cartProduct.product.price * cartProduct.quantity, 0)
);
