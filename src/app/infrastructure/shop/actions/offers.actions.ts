import { Action } from '@ngrx/store';
import { CartProductInterface } from '@domain/cart/cart-product.model';
import { CommercialOffers } from '@domain/promotion/offer.model';

export enum OffersActionTypes {
  CommercialOffersRequest = '[Cart] Commercial Offers Request',
  CommercialOffersSuccess = '[Cart] Commercial Offers Success',
  CommercialOffersFailure = '[Cart] Commercial Offers Failure',
}

export class CommercialOffersRequest implements Action {
  readonly type = OffersActionTypes.CommercialOffersRequest;
  constructor(public payload: {cartProducts: CartProductInterface[]}) {}
}

export class CommercialOffersSuccess implements Action {
  readonly type = OffersActionTypes.CommercialOffersSuccess;
  constructor(public payload: {commercialOffers: CommercialOffers}) {}
}

export class CommercialOffersFailure implements Action {
  readonly type = OffersActionTypes.CommercialOffersFailure;
  constructor(public payload: {error: Error}) {}
}

export type OffersActions =
  CommercialOffersRequest |
  CommercialOffersSuccess |
  CommercialOffersFailure;
