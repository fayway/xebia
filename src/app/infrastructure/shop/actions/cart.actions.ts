import { Action } from '@ngrx/store';
import { Product } from '@domain/product/product.model';

export enum CartActionTypes {
  AddProduct = '[Cart] Add Product',
  RemoveProduct = '[Cart] Remove Product',
  IncrementQuantity = '[Cart] Increment Product Quantity',
  DecrementQuantity = '[Cart] Decrement Product Quantity',
}

export class AddProductToCart implements Action {
  readonly type = CartActionTypes.AddProduct;
  constructor(public payload: {product: Product}) {}
}

export class RemoveProductFromCart implements Action {
  readonly type = CartActionTypes.RemoveProduct;
  constructor(public payload: {product: Product}) {}
}

export class IncrementProductQuantity implements Action {
  readonly type = CartActionTypes.IncrementQuantity;
  constructor(public payload: {product: Product}) {}
}

export class DecrementProductQuantity implements Action {
  readonly type = CartActionTypes.DecrementQuantity;
  constructor(public payload: {product: Product}) {}
}

export type CartActions =
  AddProductToCart |
  RemoveProductFromCart |
  IncrementProductQuantity |
  DecrementProductQuantity;
