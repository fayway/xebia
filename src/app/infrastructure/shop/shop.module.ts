import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CartPageComponent } from './containers/cart-page/cart-page.component';
import { ShopEffects } from './effects/shop.effects';
import { reducers } from './reducers/index';
import { SharedModule } from '@infrastructure/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    StoreModule.forFeature('shop', reducers),
    EffectsModule.forFeature([ShopEffects])
  ],
  declarations: [CartPageComponent]
})
export class ShopModule { }
