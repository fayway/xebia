import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CartProductInterface } from '@domain/cart/cart-product.model';
import { CommercialOffers} from '@domain/promotion/offer.model';
import { CommercialOffersFactory } from '@application/promotion/commercial-offers.factory';
import { OffersServiceInterface } from '@application/promotion/offers.service';
import { ExternalCommercialOffers } from '@application/promotion/external-offers.model';

@Injectable({
  providedIn: 'root'
})
export class OffersService implements OffersServiceInterface {

  private readonly booksApi = `${environment.apiPath}/books`;

  constructor(private http: HttpClient) { }

  getCommercialOffers(cartProducts: CartProductInterface[]): Observable<CommercialOffers> {
    const last = cartProducts[cartProducts.length - 1];
    const remaining = cartProducts.slice(0, -1);
    const ids = remaining.reduce((acc, cartProduct) => {
      return this.repeatCartProductId(cartProduct) + ',' + acc;
    }, this.repeatCartProductId(last));

    const url = `${this.booksApi}/${ids}/commercialOffers`;
    return this.http.get<ExternalCommercialOffers>(url).pipe(
      map(response => CommercialOffersFactory.mapToDomain(response))
    );
  }

  private repeatCartProductId(cartProduct: CartProductInterface): string {
    return Array.from(Array(cartProduct.quantity)).map(() => cartProduct.product.getId()).join(',');
  }
}
