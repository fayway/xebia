import { TestBed } from '@angular/core/testing';

import { OffersService } from './offers.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/index';
import { environment } from '@env/environment';
import { Book } from '@domain/catalog/item.model';

describe('OffersService', () => {
  let http: HttpClient;
  let service: OffersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: HttpClient, useValue: {get: jest.fn()}},
        OffersService
      ]
    });

    service = TestBed.get(OffersService);
    http = TestBed.get(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getCommercialOffers() should be called with the right isbn 1', async () => {
    http.get = jest.fn(() => of({offers: []}));
    await service.getCommercialOffers([
      {product: new Book('123', 34), quantity: 2},
    ]).toPromise();
    expect(http.get).toHaveBeenCalledWith(`${environment.apiPath}/books/123,123/commercialOffers`);
  });

  it('getCommercialOffers() should be called with the right isbn 2', async () => {
    http.get = jest.fn(() => of({offers: []}));
    await service.getCommercialOffers([
      {product: new Book('123', 34), quantity: 2},
      {product: new Book('456', 12), quantity: 1},

    ]).toPromise();
    expect(http.get).toHaveBeenCalledWith(`${environment.apiPath}/books/123,123,456/commercialOffers`);
  });
});
