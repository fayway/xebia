import { MinusOffer } from '@domain/promotion/minus-offer.model';
import { OfferType, OfferError } from '@domain/promotion/offer.model';

export class MinusOfferAdapter {
  static mapToDomain(obj: { type: string, value: number }): MinusOffer {
    if (obj.type !== OfferType.MINUS) {
      throw new OfferError(`Expected Offer.type ${OfferType.MINUS}, got obj.type ${obj.type}`);
    }
    return new MinusOffer(obj.value);
  }
}
