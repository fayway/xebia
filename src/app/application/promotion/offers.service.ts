import { Observable } from 'rxjs';
import { CommercialOffers } from '@domain/promotion/offer.model';
import { CartProductInterface } from '@domain/cart/cart-product.model';

export interface OffersServiceInterface {
  getCommercialOffers(cartProducts: CartProductInterface[]): Observable<CommercialOffers>;
}
