import { OfferType } from '@domain/promotion/offer.model';

export interface ExternalCommercialOffers {
  offers: ExternalOffer[];
}

export interface ExternalOffer {
  type: OfferType;
  value: number;
  sliceValue: number;
}
