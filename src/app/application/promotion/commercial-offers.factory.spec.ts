import { CommercialOffersFactory } from './commercial-offers.factory';
import { MinusOffer } from '@domain/promotion/minus-offer.model';
import { PercentageOffer } from '@domain/promotion/percentage-offer.model';
import { SliceOffer } from '@domain/promotion/slice-offer.model';
import { ExternalCommercialOffers } from '@application/promotion/external-offers.model';

describe('CommercialOffersFactory', () => {

  it('should return the right Offer instance', () => {
    const json = {
      offers: [
        {type: 'minus', value: 15},
        {type: 'percentage', value: 5},
        {type: 'slice', sliceValue: 100, value: 12}
      ]
    };

    const offers = CommercialOffersFactory.mapToDomain(json).offers;

    expect(offers.length).toBe(3);
    expect(offers[0]).toBeInstanceOf(MinusOffer);
    expect(offers[1]).toBeInstanceOf(PercentageOffer);
    expect(offers[2]).toBeInstanceOf(SliceOffer);
  });

});
