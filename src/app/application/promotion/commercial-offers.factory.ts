import { CommercialOffers, OfferType } from '@domain/promotion/offer.model';
import { ExternalCommercialOffers } from './external-offers.model';
import { MinusOfferAdapter } from '@application/promotion/minus-offer.adapter';
import { PercentageOfferAdapter } from '@application/promotion/percentage-offer.adapter';
import { SliceOfferAdapter } from '@application/promotion/slice-offer.adapter';


export class CommercialOffersFactory {
  static mapToDomain(obj: ExternalCommercialOffers): CommercialOffers {
    const offers = obj.offers.map(offer => {
      switch (offer.type) {
        case OfferType.PERCENTAGE:
          return PercentageOfferAdapter.mapToDomain(offer);
        case OfferType.MINUS:
          return MinusOfferAdapter.mapToDomain(offer);
        case OfferType.SLICE:
          return SliceOfferAdapter.mapToDomain(offer);
        default:
          return undefined;
        // throw new OfferError(`Unexpected offer.type ${offer.type}`);
      }
    });
    return new CommercialOffers(offers);
  }
}
