import { SliceOffer } from '@domain/promotion/slice-offer.model';
import { OfferType, OfferError } from '@domain/promotion/offer.model';

export class SliceOfferAdapter {

  static mapToDomain(obj: { type: string, sliceValue: number, value: number }): SliceOffer {
    const {type, sliceValue, value} = obj;
    if (type !== OfferType.SLICE) {
      throw new OfferError(`Expected Offer.type ${OfferType.MINUS}, got obj.type ${obj.type}`);
    }
    return new SliceOffer(sliceValue, value);
  }
}
