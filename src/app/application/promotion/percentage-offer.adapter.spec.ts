import { PercentageOfferAdapter } from '@application/promotion/percentage-offer.adapter';
import { OfferError, OfferType } from '@domain/promotion/offer.model';

describe('PercentageOffer Domain', () => {

  test('mapToDomain()', () => {
    const offer = PercentageOfferAdapter.mapToDomain({type: 'percentage', value: 5});
    expect(offer.getType()).toBe(OfferType.PERCENTAGE);
    expect(offer.value).toBe(5);
  });

  test('mapToDomain() conversion Error', () => {
    expect(() => PercentageOfferAdapter.mapToDomain({type: 'minus', value: 5})).toThrow(OfferError);
  });

});
