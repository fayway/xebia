import { SliceOfferAdapter } from '@application/promotion/slice-offer.adapter';
import { OfferError, OfferType } from '@domain/promotion/offer.model';

describe('SliceOffer Domain', () => {

  test('mapToDomain()', () => {
    const offer = SliceOfferAdapter.mapToDomain({type: 'slice', sliceValue: 100, value: 12});
    expect(offer.getType()).toBe(OfferType.SLICE);
    expect(offer.sliceValue).toBe(100);
    expect(offer.value).toBe(12);
  });


  test('mapToDomain() conversion Error', () => {
    expect(() => SliceOfferAdapter.mapToDomain({type: 'minus', sliceValue: 100, value: 5}))
      .toThrow(OfferError);
  });
});

