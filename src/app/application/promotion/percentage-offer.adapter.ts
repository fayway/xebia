import { PercentageOffer } from '@domain/promotion/percentage-offer.model';
import { OfferType, OfferError } from '@domain/promotion/offer.model';

export class PercentageOfferAdapter {
  static mapToDomain(obj: { type: string, value: number }): PercentageOffer {
    if (obj.type !== OfferType.PERCENTAGE) {
      throw new OfferError(`Expected Offer.type ${OfferType.PERCENTAGE}, got obj.type ${obj.type}`);
    }
    return new PercentageOffer(obj.value);
  }
}
