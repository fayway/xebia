import { MinusOfferAdapter } from '@application/promotion/minus-offer.adapter';
import { OfferError, OfferType } from '@domain/promotion/offer.model';

describe('MinusOfferAdapter', () => {

  test('mapToDomain()', () => {
    const offer = MinusOfferAdapter.mapToDomain({type: 'minus', value: 15});
    expect(offer.getType()).toBe(OfferType.MINUS);
    expect(offer.value).toBe(15);
  });

  test('mapToDomain() conversion Error', () => {
    expect(() => MinusOfferAdapter.mapToDomain({type: 'slice', value: 15})).toThrow(OfferError);
  });

});
