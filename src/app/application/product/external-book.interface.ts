export interface ExternalBookInterface {
  isbn: string;
  title: string;
  price: number;
  cover: string;
  synopsis: string[];
}
