import { Observable } from 'rxjs';
import { ExternalBookInterface } from './external-book.interface';

export interface BooksServiceInterface {
  getBooks(): Observable<ExternalBookInterface[]>;
  search(books: ExternalBookInterface[], query: string): Observable<ExternalBookInterface[]>;
}
