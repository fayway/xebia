import { ExternalBookInterface } from '@application/product/external-book.interface';
import { Book } from '@domain/catalog/item.model';

export class BookAdapter {
  static mapToDomain(book: ExternalBookInterface): Book {
    return new Book(book.isbn, book.price, book.title, book.cover, book.synopsis);
  }
}
