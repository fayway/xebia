import { Book } from '@domain/catalog/item.model';
import { BookAdapter } from '@application/product/book.adapter';
import { ExternalBookInterface } from '@application/product/external-book.interface';

describe('BookAdapter', () => {
  test('mapToDomain() should cast json obj to Book domain', () => {
    const obj: ExternalBookInterface = {
      'isbn': 'bbcee412-be64-4a0c-bf1e-315977acd924',
      'title': 'Henri Potier et les Reliques de la Mort',
      'price': 35,
      'cover': 'http://henri-potier.xebia.fr/hp6.jpg',
      'synopsis': [
        'Lorem'
      ]
    };
    const book = BookAdapter.mapToDomain(obj);
    expect(book instanceof Book).toBeTruthy();
    expect(book.getId()).toBe(obj.isbn);
    expect(book.isbn).toBe(obj.isbn);
    expect(book.title).toBe(obj.title);
    expect(book.price).toBe(obj.price);
    expect(book.cover).toBe(obj.cover);
    expect(book.synopsis).toBe(obj.synopsis);
  });
});
